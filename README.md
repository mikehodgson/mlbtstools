# README #

This repository contains tools I've developed for working with MLB The Show data

### Current Tools ###

* ddscraper.rb - Ruby script for getting a dump of my currently owned cards

### Running Scripts ###

#### From Release ####

* Download latest release from [Project Downloads](https://bitbucket.org/mikehodgson/mlbtstools/downloads/)
* Unzip into a directory
* Execute program at command prompt, example:

```
ddscraper.exe <PSN Username> <PSN Password>
```

#### From Source ####

* Install [Ruby](http://ruby-lang.org/) and make sure it is in your PATH
* Install Bundler Gem

```
gem install bundler
```

* Install bundled gems

```
bundle install
```

* Execute scripts in the terminal / command prompt, example:

```
ruby ddscraper.rb <PSN Username> <PSN Password>
```

### License ###

MIT License

Copyright (c) 2017 Michael Hodgson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.