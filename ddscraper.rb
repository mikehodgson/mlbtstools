# MIT License
# 
# Copyright (c) 2017 Michael Hodgson
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

require 'openssl'
require 'mechanize'
require 'csv'
require 'fileutils'
require 'color-console'

# We don't care about SSL cert verification
$VERBOSE = nil
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE
I_KNOW_THAT_OPENSSL_VERIFY_PEER_EQUALS_VERIFY_NONE_IS_WRONG = nil

#Global variables
$VERSION = 0.2
$PROGRAM_NAME = "ddscraper"
$COPYRIGHT = "(c) 2017 Michael Hodgson. See README.md for Licensing"
$SERIES = ['live+series','flashbacks']
$TEAM_NAMES = ['Orioles','Red Sox','Yankees','Rays','Blue Jays','White Sox','Indians','Tigers','Royals','Twins','Angels','Athletics','Mariners','Rangers','Astros','Braves','Marlins','Nationals','Mets','Phillies','Cubs','Reds','Brewers','Pirates','Cardinals','Diamondbacks','Rockies','Dodgers','Padres','Giants']
$MAX_TEAMS = $TEAM_NAMES.length

$agent = Mechanize.new { |agent|
	agent.follow_meta_refresh = true
}

def banner
	Console.title = "#{$PROGRAM_NAME} v#{$VERSION}"
	puts "#{$PROGRAM_NAME} v#{$VERSION}"
	puts "#{$COPYRIGHT}"
end

# Save data to file
def save_data(data)
	filename = "cards_#{Time.now.strftime('%Y-%m-%d_%H-%M-%S')}.csv"
	dirname = File.dirname(filename)
	lastcolumn = {:id => '', :name => '', :team => '', :owned => 0, :base_rating => '', :ie_boost => '', :effective_rating => '', :position => '', :buy_price => 0, :sell_price => 0, :throws => '', :bats => '', :series => ''}
	column_names = data.first.keys
	s = CSV.generate do |csv|
		csv << column_names
		data.each do |p|
			csv << p.values
			lastcolumn[:owned] += p[:owned]
			lastcolumn[:buy_price] += (p[:buy_price] * p[:owned])
			lastcolumn[:sell_price] += (p[:sell_price] * p[:owned])
		end
		csv << lastcolumn.values
	end
	unless File.directory?(dirname)
	  	FileUtils.mkdir_p(dirname)
	end
	File.write(filename, s)
	puts "Records saved to #{filename}"
end

# Get owned players listing
def get_owned_players
	players = []
	next_page_available = true
	$agent.get('https://theshownation.com/sessions/login') do |homepage|
		page = homepage.form_with(:id => 'signInForm') do |form|
			form.j_username = ARGV[0]
			form.j_password = ARGV[1]
		end.submit

		if (page.title == 'MLB The Show')
			Console.puts 'Login Successful :)', :green
			current = {:page => 1, :team => 0, :series => 'live+series', :url => ''}
			loop do
				current_progress = ((current[:team].to_f / $MAX_TEAMS.to_f) * 100.0).to_f
				Console.show_progress("Scanning team #{$TEAM_NAMES[current[:team]]}", current_progress.to_i)
				$SERIES.each do |currentSeries|
					current[:series] = currentSeries
					loop do
						current[:url] = 'http://theshownation.com/inventory?filter_series=' + current[:series] + '&filter_special=' + current[:team].to_s + '&filter_type=Mlb+Player&filter_view=owned&page=' + current[:page].to_s
						$agent.get(current[:url]) do |inventory_page|
							# puts "Getting content for team: #{$TEAM_NAMES[current[:team]]}, page: #{current[:page]}, series: #{current[:series]}"
							doc = inventory_page.parser
							doc.search('table.marketplace-search tbody tr').each do |tr|
								cells = tr.search("td")
								if (cells.last.search('a').length > 0)
									row_data = {:id => 0, :name => '', :team => '', :owned => 0, :base_rating => 0, :ie_boost => 0, :effective_rating => 0, :position => '', :buy_price => 0, :sell_price => 0, :throws => '', :bats => '', :series => current[:series]}
									row_data[:name] =  cells.last.search('a').first.text.strip
									row_data[:team] =  $TEAM_NAMES[current[:team]]
									row_data[:id] = cells.last.search('input[name="item_ref_id"]').first['value'].to_i
									row_data[:owned] = cells[2].text.strip.to_i
									row_data[:buy_price] = cells.last.search('form[action="/marketplace/create_buy_now"] input[name="price"]').first['value'].to_i
									row_data[:sell_price] = cells.last.search('form[action="/marketplace/create_sell_now"] input[name="price"]').first['value'].to_i
									players << row_data
								end
							end
							if (doc.search('a.next_page').length > 0)
								next_page_available = true
								current[:page] += 1
							else
								next_page_available = false
								current[:page] = 1
							end
						end
						break if next_page_available == false
					end
				end
				current[:team] += 1
				break if current[:team] == $MAX_TEAMS
			end
			Console.clear_progress
			return players
		else
			Console.puts 'Login Failed :(', :red
			return nil
		end
	end
end

# Parse detail pages to get extra info
def get_player_details(players)
	new_players = []
	players.each do |player|
		current_progress = ((new_players.length.to_f / players.length.to_f) * 100.0).to_f
		Console.show_progress("Scanning player #{player[:name]}", current_progress.to_i)
		# puts current_progress
		# print "Getting details for ... "
		url = "http://theshownation.com/marketplace/listing?item_ref_id=#{player[:id]}"
		begin
			$agent.get(url) do |player_page|
				doc = player_page.parser
				tr = doc.search('div.listing-name-info').first.next_element.search('tr')[1]
				cells = tr.search('td')
				player[:base_rating] = cells[0].text.strip.split(" ").first.to_i
				if (cells[0].search('i').length > 0)
					player[:effective_rating] = cells[0].text.strip.split(" ").last.to_i
				else
					player[:effective_rating] = player[:base_rating].to_i
				end
				player[:ie_boost] = player[:effective_rating].to_i - player[:base_rating].to_i
				player[:position] = cells[1].text.strip
				player[:bats] = cells[2].text.strip
				player[:throws] = cells[3].text.strip

			end
		rescue Net::HTTPInternalServerError
			# puts "error."
		rescue Mechanize::ResponseCodeError
			# puts "error."
		end
		# puts "done"
		new_players << player
	end
	Console.clear_progress
	return new_players
end

banner()

if (ARGV.length < 2)
	puts "Usage: ddscraper.exe <PSN Username> <PSN Password>"
else
	myplayers = get_owned_players()
	if (myplayers != nil)
		myplayers = get_player_details(myplayers)
		save_data(myplayers)
	end
end